'use strict';
document.addEventListener('DOMContentLoaded', () => {

  // Tabs
  const tabsWrap = document.querySelector('.cars__tabs'),
    tabs         = document.querySelectorAll('.cars__tab'),
    blocks       = document.querySelectorAll('.cars__block');

  const hideBlocks = () => {
    blocks.forEach((block) => {
      block.classList.remove('active');
    });
    tabs.forEach((tab) => {
      tab.classList.remove('active');
    });
  };
  hideBlocks();
  const showBlock = (i = 0) => {
    hideBlocks();
    tabs[i].classList.add('active');
    blocks[i].classList.add('active');
  };
  showBlock();

  tabsWrap.addEventListener('click', (e) => {
    const target = e.target;
    if (target && target.classList.contains('cars__tab')) {
      tabs.forEach((tab, index) => {
        if (tab === target) {
          showBlock(index);
        }
      });
    }
  });

  // get data
  const processBlocks = document.querySelector('.process__blocks');

  const render = (data) => {
    data.forEach((item) => {
      const element = document.createElement('div');
      element.innerHTML = `
        <div class="process__block block-process">
          <div class="block-process__img">
            <img src="${item.img}" alt="">
          </div>
          <div class="block-process__wrap">
              <div class="block-process__title">
                ${item.title}
              </div>
              <div class="block-process__desc">
                ${item.desc}
              </div>
          </div>
        </div>
      `;
      processBlocks.append(element);
    });
  };
  fetch('http://localhost:3000/data')
    .then((data) => data.json())
    .then((res) => render(res));

  // post data
  const form = document.querySelector('.main-form'),
    label = document.querySelector('.main-form__label');

  form.addEventListener('submit', (e) => {
    e.preventDefault();
    const formData = {
      name: form.name.value,
      phone: form.phone.value,
      agree: form.agree.checked
    };
    if (form.agree.checked) {
      label.style.color = 'black';
      fetch('http://localhost:3000/orders', {
        method: 'POST',
        body: JSON.stringify(formData),
        headers: {
          'Content-type': 'application/json'
        }
      });
    } else {
      label.style.color = 'red';
    }
  });

  // Slider
  //Pocedural
  // const slides   = document.querySelectorAll('.reviews__block'),
  //   slideNext    = document.querySelector('.reviews__slider-next'),
  //   slidePrev    = document.querySelector('.reviews__slider-prev');
  // let indexSlide = 1;

  // const updateSlide = (n) => {
  //   if (n > slides.length) {
  //     indexSlide = 1;
  //   } else if (n < 1) {
  //     indexSlide = slides.length;
  //   }
  //   slides.forEach((slide) => slide.style.display = 'none');
  //   slides[indexSlide - 1].style.display = 'block';
  // };
  // updateSlide(indexSlide);

  // const changeSlide = (n) => {
  //   indexSlide += n;
  //   updateSlide(indexSlide);
  // };

  // slideNext.addEventListener('click', () => changeSlide(1));
  // slidePrev.addEventListener('click', () => changeSlide(-1));

  //func obj
  // const slides   = document.querySelectorAll('.reviews__block'),
  //   slideNext    = document.querySelector('.reviews__slider-next'),
  //   slidePrev    = document.querySelector('.reviews__slider-prev');

  // const Slider = (slides, slideNext, slidePrev, indexSlide = 1) => ({
  //   slides, slideNext, slidePrev, indexSlide,
  //   updateSlide(n) {
  //     if (n > slides.length) {
  //       this.indexSlide = 1;
  //     } else if (n < 1) {
  //       this.indexSlide = this.slides.length;
  //     }
  //     this.slides.forEach((slide) => slide.style.display = 'none');
  //     this.slides[this.indexSlide - 1].style.display = 'block';
  //   },
  //   changeSlide(n) {
  //     this.indexSlide += n;
  //     this.updateSlide(this.indexSlide);
  //   },
  //   emit() {
  //     slideNext.addEventListener('click', () => this.changeSlide(1));
  //     slidePrev.addEventListener('click', () => this.changeSlide(-1));
  //   }
  // });
  // const newSlier = Slider(slides, slideNext, slidePrev);
  // newSlier.updateSlide(1);
  // newSlier.emit();

  //OOP
  const slides   = document.querySelectorAll('.reviews__block'),
    slideNext    = document.querySelector('.reviews__slider-next'),
    slidePrev    = document.querySelector('.reviews__slider-prev');

  const Slider = class {
    constructor(slides, slideNext, slidePrev, indexSlide = 1) {
      this.slides = slides;
      this.slideNext = slideNext;
      this.slidePrev = slidePrev;
      this.indexSlide = indexSlide;
    }
    updateSlide(n) {
      if (n > slides.length) {
        this.indexSlide = 1;
      } else if (n < 1) {
        this.indexSlide = this.slides.length;
      }
      this.slides.forEach((slide) => slide.style.display = 'none');
      this.slides[this.indexSlide - 1].style.display = 'block';
    }
    changeSlide(n) {
      this.indexSlide += n;
      this.updateSlide(this.indexSlide);
    }
    emit() {
      slideNext.addEventListener('click', () => this.changeSlide(1));
      slidePrev.addEventListener('click', () => this.changeSlide(-1));
    }
  };
  const newSlier = new Slider(slides, slideNext, slidePrev);
  newSlier.updateSlide(1);
  newSlier.emit();
});
